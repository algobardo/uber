
NAME = "algobardo/uber-history"
NAME_WWW = "algobardo/uber-history-www"
VER = "v1.0"

docker-build: build; cp target/scala-2.12/history.jar docker/history.jar; cp -rf html docker/; (cd docker && docker build -t $(NAME):$(VER) . && docker build -f Dockerfile-www -t $(NAME_WWW):$(VER) .);

docker-publish: docker-build; (cd docker && docker push $(NAME):$(VER) && docker push $(NAME_WWW):$(VER))

build: ;sbt assembly

tests: ;sbt test

start-stack: ;(cd docker && docker stack deploy --compose-file docker-swarm.yml history)

stop-stack: ;docker stack rm history

log-workers: ;docker service logs -f history_worker

log-dispatchers: ;docker service logs -f history_dispatcher

.PHONY:	build
