const DEBUG = false;
const SERVICE_ENDPOINT = DEBUG ? "http://localhost:8080" : "http://35.204.106.25:8080";

$(document).ready(function () {

    const commitsIds = ['#commits-template', '#commits-result'];
    const commitsTemplate = Handlebars.compile($(commitsIds[0]).html());
    const namesRegexp = /^[a-zA-Z0-9_-]+$/;

    function updateUiWaitResponse() {
        $("#commits-section").hide();
        $("#alert").hide();
        $("#show").prop("disabled", true);
        $("#spin").show();
    }

    function updateUiResponseFail(msg) {
        $("#commits-section").hide();
        $("#alert").show().text(msg);
        $("#show").prop("disabled", false);
        $("#spin").hide();
    }

    function updateUiResponseSuccess(commits) {
        // showing the commits section
        $("#commits-section").show();
        $("#alert").hide();
        $("#show").prop("disabled", false);
        $("#spin").hide();

        // inserting the data into the template and show them
        let html = commitsTemplate(commits);
        $(commitsIds[1]).html(html)
    }

    function query(userName, repoName, pageNumber) {
        let url = SERVICE_ENDPOINT + "/history/" + userName + "/" + repoName + "/" + pageNumber;
        $.ajax({
            url: url,
            error: function (err) {
                updateUiResponseFail("Error '" + err.statusText + "' while processing the request");
                console.error(err);
            },
            success: function (data) {
                if (typeof data !== 'object') {
                    updateUiResponseFail("Bad response");
                } else if (data.HistorySuccess) {
                    updateUiResponseSuccess(data.HistorySuccess);
                } else if (data.NonExistingRepo) {
                    updateUiResponseFail("Repository does not exists");
                } else if (data.RetryLater) {
                    updateUiResponseFail("Retry later, the service is processing your request");
                }
            },
            timeout: 60000 // 60 seconds

        });
    }

    function isValidInput(userName, repoName, pageNumber) {
        return typeof userName === 'string' && namesRegexp.test(userName)
            && typeof repoName === 'string' && namesRegexp.test(repoName)
            && pageNumber !== '' && !isNaN(pageNumber)
    }


    // register event when submitting a show request
    $("#query").submit(function (event) {
        event.preventDefault();
        let userName = $("#userName").val().trim();
        let repoName = $("#repoName").val().trim();
        let pageNumber = $("#pageNumber").val().trim();

        if (!isValidInput(userName, repoName, pageNumber)) {
            $("#alert").show().text("Check your input!");
        } else {
            pageNumber = +pageNumber;
            updateUiWaitResponse();
            query(userName, repoName, pageNumber);
        }
    });
});