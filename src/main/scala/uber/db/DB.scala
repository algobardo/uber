package uber.db

import org.log4s.getLogger
import slick.dbio.{DBIOAction, NoStream}
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.{ExecutionContext, Future}

/**
  * A database instance, initialized with the configuration `configName`.
  */
class DB(configName: String)(implicit ec: ExecutionContext) {
  private val log = getLogger

  private val instance = Database.forConfig(configName)

  def run[R](a: DBIOAction[R, NoStream, Nothing]): Future[R] = {
    instance.run(a)
  }

  /**
    * Closes the underlying db instance.
    */
  def close(): Unit = {
    instance.close()
  }
}
