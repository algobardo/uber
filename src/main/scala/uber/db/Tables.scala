package uber.db

import org.log4s.getLogger
import slick.jdbc.MySQLProfile
import uber.GithubKnowledge

import scala.concurrent.{ExecutionContext, Future}

/**
  * Tables in db and utilities to create schemas for them.
  */
object Tables {
  import MySQLProfile.api._

  private val log = getLogger

  type WorkersEntry = (String, String, String)

  /**
    * The table contains the IP of the latest worker that has resolved a query for the repository `repo` of the `user`.
    */
  class WorkersTable(tag: Tag) extends Table[WorkersEntry](tag, "WORKERS") {

    /**
      * The username of the repository owner.
      */
    def user = column[String]("USER", O.Length(GithubKnowledge.MAX_USER_LENGTH)) // Known github username length

    /**
      * The name of the repository.
      */
    def repo = column[String]("REPO", O.Length(GithubKnowledge.MAX_REPO_LENGTH)) // Known github repository name length

    /**
      * The IP of the latest worker that has resolved a query for the repository `repo` of the `user`.
      */
    def workerIp = column[String]("WORKER_IP", O.Length(15)) // IP length

    def * = (user, repo, workerIp)

    def pk = primaryKey("pk", (user, repo))

  }
  val WorkersTable = TableQuery[WorkersTable]

  /**
    * Create the schema in the database for the tables in `Tables`.
    */
  def initSchemas(db: DB)(implicit ec: ExecutionContext): Future[Unit] = {
    import MySQLProfile.api._

    val setup = DBIO.seq(Tables.WorkersTable.schema.create)
    db.run(setup)
      .map { _ =>
        log.info("Successfully created schemas")
      }
  }

}
