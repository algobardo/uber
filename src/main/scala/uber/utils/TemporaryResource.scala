package uber.utils

import org.log4s.getLogger

import scala.collection.mutable
import scala.util.Try

/**
  * A resource that needs to be freed within the JVM lifecycle.
  */
trait TemporaryResource[T] {
  override protected def finalize(): Unit

  /**
    * Free the temporary resource
    */
  def cleanup(): Unit
}

/**
  * Manages globally temporary resources status
  */
object TemporaryResourceHandler {

  private val registeredResources: mutable.WeakHashMap[TemporaryResource[_], Unit] = mutable.WeakHashMap()

  private val log = getLogger

  /**
    * Register a resource for cleanup.
    */
  def registerForCleanup[T](res: TemporaryResource[T]): Unit = this.synchronized {
    registeredResources.put(res, ())
  }

  /**
    * Un-register a resource from the clenup.
    */
  def unregisterForCleanup[T](res: TemporaryResource[T]): Unit = this.synchronized {
    registeredResources.remove(res)
  }

  sys.addShutdownHook {
    log.info("JVM shutdown, freeing temporary resources")
    val itemsToCleanup = registeredResources.synchronized {
      registeredResources.keys.toList
    }
    itemsToCleanup.foreach(res => Try(res.cleanup()))
  }
}
