package uber.utils

import java.util.concurrent.TimeoutException
import java.util.{Timer, TimerTask}

import scala.concurrent.duration.Duration
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.language.postfixOps

/**
  * Future utilities
  */
object FutureUtils {

  /**
    * Simple timeout for scala futures.
    *
    * all the credits to http://justinhj.github.io/2017/07/16/future-with-timeout.html
    */
  implicit class FutureWithTimeout[T](future: Future[T]) {

    val timer: Timer = new Timer(true)

    /**
      * Returns the result of the provided future within the given time or a timeout exception, whichever is first
      * This uses Java Timer which runs a single thread to handle all futureWithTimeouts and does not block like a
      * Thread.sleep would
      * The timeout parameter is the time before we return a Timeout exception instead of future's outcome.
      *
      * Note that the future will run to completion.
      */
    def withTimeout(timeout: Duration)(implicit ec: ExecutionContext): Future[T] = {

      // Promise will be fulfilled with either the callers Future or the timer task if it times out
      var p = Promise[T]

      // and a Timer task to handle timing out
      val timerTask = new TimerTask() {
        def run(): Unit = {
          p.tryFailure(new TimeoutException())
        }
      }

      // Set the timeout to check in the future
      if (timeout.isFinite())
        timer.schedule(timerTask, timeout.toMillis)

      future
        .map { a =>
          if (p.trySuccess(a)) {
            timerTask.cancel()
          }
        }
        .recover {
          case e: Exception =>
            if (p.tryFailure(e)) {
              timerTask.cancel()
            }
        }

      p.future
    }

  }

  /**
    * Utility class to retry procedures constructing futures until the future complete with success.
    */
  implicit class Retries[T](op: () => Future[T]) {

    /**
      * Execute the future constructed by `op`, if it fails and `toRetry` returns true on the given exception
      * then a new future is constructed.
      * At most `retries` futures are constructed.
      */
    def retry(toRetry: Throwable => Boolean, retries: Int)(implicit ec: ExecutionContext): Future[T] = {
      val future = op()
      if (retries >= 0) {
        future recoverWith {
          case x =>
            if (toRetry(x)) retry(toRetry, retries - 1)
            else future
        }
      } else {
        future
      }
    }
  }
}
