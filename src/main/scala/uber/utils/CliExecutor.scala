package uber.utils

import java.nio.file.Path
import java.util.concurrent.TimeoutException

import uber.utils.FutureUtils._

import scala.concurrent._
import scala.concurrent.duration.Duration
import scala.sys.process.{Process, ProcessLogger}

/**
  * Utilities for running command line processes.
  */
object CliExecutor {

  case class BadErrorCodeException(code: Int, log: String) extends RuntimeException()

  /**
    * The result of a process execution where the exit code is `code`
    * and the stdout and stderr is interleaved in the `log` string.
    */
  case class ProcessExecutionResult(code: Int, log: String)

  /**
    * A process logger that logs into a string.
    */
  case class StrLogger() extends ProcessLogger {
    private[this] val stringBuffer: StringBuffer = new StringBuffer()

    def out(s: => String): Unit = {
      stringBuffer.append(s)
      stringBuffer.append("\n")
    }

    def err(s: => String): Unit = {
      stringBuffer.append(s)
      stringBuffer.append("\n")
    }

    def buffer[T](f: => T): T = f

    def result: String = stringBuffer.toString

  }

  /**
    * Execute the given command in the given working directory, and the given timeout in the future.
    * The future fails with `BadErrorCodeException` if the process returns a code != 0 and `throwOnErrorCode` is true.
    */
  def executeCommand(cmd: Seq[String], cwd: Path, timeout: Duration = Duration.Inf, throwOnErrorCode: Boolean = false)(
    implicit ec: ExecutionContext
  ): Future[ProcessExecutionResult] = {

    val logger = StrLogger()
    val proc =
      Process(command = cmd, cwd = cwd.toAbsolutePath.toFile).run(logger)

    Future(blocking(proc.exitValue()))
      .withTimeout(timeout)
      .map { code =>
        if (code == 0 || !throwOnErrorCode) ProcessExecutionResult(code, logger.result)
        else throw BadErrorCodeException(code, logger.result)
      }
      .recover {
        case e: TimeoutException =>
          proc.destroy()
          throw e
      }
  }

}
