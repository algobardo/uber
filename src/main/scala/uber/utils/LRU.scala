package uber.utils

/**
  * An LRU map with maximum size `maxSize`.
  */
class LRU[A, B](maxSize: Int) extends java.util.LinkedHashMap[A, B] {
  override def removeEldestEntry(eldest: java.util.Map.Entry[A, B]): Boolean = size > maxSize

  /**
    * Optionally returns the value associated with the key `k`.
    */
  def getOpt(k: A): Option[B] = {
    if (containsKey(k))
      Some(get(k))
    else
      None
  }
}
