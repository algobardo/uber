package uber.query_service

import java.net.URL
import java.time.{Duration, Instant}

import org.log4s.getLogger
import uber.GithubKnowledge
import uber.git.{GitConfiguration, GitRepoDoesNotExists, LocalGitRepository}
import uber.utils.FutureUtils._
import uber.utils.LRU

import scala.concurrent.{Future, TimeoutException}

/**
  * A query service implementation that uses local git repositories to answer queries.
  */
class QueryServiceImpl(implicit conf: QueryServiceConfiguration, gitConf: GitConfiguration) extends QueryService {

  import conf._

  private val log = getLogger

  /**
    * A cache of local git repositories futures.
    * Note: The futures might be completed or not.
    */
  private val repoCache: LRU[String, Future[LocalGitRepository]] = new LRU(conf.CACHED_REPOS)

  override def history(user: UserName, repo: RepoName, page: Int): Future[HistoryResponse] = {
    val from = conf.PAGE_SIZE * page
    val repoUrl = GithubKnowledge.getCloneUrl(user, repo)
    getCachedRepo(repoUrl, LocalGitRepository.clone(repoUrl))
      .flatMap(fetchIfNecessary)
      .flatMap(_.listCommits(from, conf.PAGE_SIZE))
      .map(HistorySuccess)
      .withTimeout(conf.TIMEOUT_RETRY_LATER)
      .recover {
        case _: GitRepoDoesNotExists => NonExistingRepo("Remote repository does not exists")
        case _: TimeoutException     => RetryLater()
      }
  }

  /**
    * Returns the number of repository in the cache.
    */
  def cacheSize: Int = this.synchronized(repoCache.size)

  /**
    * Fetch the repository `repo` if the `FETCH_INTERVAL` has elapsed.
    */
  private def fetchIfNecessary(repo: LocalGitRepository): Future[LocalGitRepository] = {
    val shouldFetch = repo.getLastFetch.forall { when =>
      Duration.between(when, Instant.now()).compareTo(Duration.ofMillis(conf.FETCH_INTERVAL.toMillis)) > 0
    }
    if (shouldFetch)
      repo.fetch()
    else
      Future.successful(repo)
  }

  /**
    * Returns the future of the repository in the cache, if present,
    * otherwise `orElse` is executed, cache is updated and future repository is returned.
    */
  private def getCachedRepo(url: URL, orElse: => Future[LocalGitRepository]): Future[LocalGitRepository] = this.synchronized {
    log.info(s"Retrieving cached repo for $url")
    val cachedItem = repoCache.getOpt(url.toString)

    if (cachedItem.isDefined) log.info(s"Repository $url is in the cache")
    else log.info(s"Repository $url is not in the cache")

    val res = cacheRepo(url, cachedItem.getOrElse(orElse))
    res.failed.foreach { e =>
      log.warn(s"Future retrieving the repository $url failed with error: $e")
      uncacheRepo(url)
    }
    res
  }

  /**
    * The repository `url` is removed from the cache.
    */
  private def uncacheRepo(url: URL): Unit = this.synchronized {
    log.info(s"Removing $url from the cache")
    repoCache.remove(url)
  }

  /**
    * The repository `repo` is stored in the cache with for the `url` key.
    */
  private def cacheRepo(url: URL, repo: Future[LocalGitRepository]): Future[LocalGitRepository] = this.synchronized {
    repoCache.put(url.toString, repo)
    repo
  }
}
