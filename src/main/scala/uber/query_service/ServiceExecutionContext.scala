package uber.query_service

import org.http4s.client.Client
import org.http4s.client.blaze.{BlazeClientConfig, PooledHttp1Client}
import uber.db.DB

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

trait ServiceExecutionContext {

  /**
    * HTTP client to perform requests.
    */
  val httpClient: Client

  /**
    * A db instance.
    */
  val db: DB

}

/**
  * A context where we can connect with the DB and perform HTTP requests
  */
object RealWorldExecutionContext extends ServiceExecutionContext {

  override val httpClient = PooledHttp1Client(
    config = BlazeClientConfig.defaultConfig.copy(requestTimeout = 40.seconds, responseHeaderTimeout = 20.seconds)
  )

  override val db = new DB("dbconfig")

}
