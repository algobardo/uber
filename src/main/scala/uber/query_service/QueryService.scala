package uber.query_service

import uber.git.GitCommit

import scala.concurrent.duration.Duration
import scala.concurrent.{ExecutionContext, Future}

/**
  * Interface of the query service.
  */
trait QueryService {

  /**
    * Returns the `page`-th page of the commits for the repository `repo` of the `user`.
    *
    * If the repository does not exists on github it returns `NonExistingRepo`,
    * if the requests takes too much time it returns `RetryLater`.
    */
  def history(user: UserName, repo: RepoName, page: Int): Future[HistoryResponse]
}

/**
  * Name of a repository.
  */
case class RepoName(name: String) extends AnyVal {
  override def toString: String = name
}

/**
  * Name of a user.
  */
case class UserName(name: String) extends AnyVal {
  override def toString: String = name
}

/**
  * Response from any query by the query service.
  */
sealed trait QueryResponse

/**
  * Response of the history query.
  */
sealed trait HistoryResponse extends QueryResponse

/**
  * A successful response of the history query.
  */
case class HistorySuccess(commits: List[GitCommit]) extends HistoryResponse

/**
  * The repository does not exists on github.
  */
case class NonExistingRepo(msg: String) extends HistoryResponse

/**
  * Retry later, the retrieval is taking too much time.
  */
case class RetryLater() extends HistoryResponse

/**
  * Configuration of a query service
  */
trait QueryServiceConfiguration {

  /**
    * Page size.
    */
  def PAGE_SIZE: Int

  /**
    * Number of repository cached.
    */
  def CACHED_REPOS: Int

  /**
    * Minimum between two fetches of a git repository.
    */
  def FETCH_INTERVAL: Duration

  /**
    * Timeout for getting a `RetryLater` response.
    */
  def TIMEOUT_RETRY_LATER: Duration

  implicit def ec: ExecutionContext
}
