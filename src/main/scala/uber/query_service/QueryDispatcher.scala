package uber.query_service

import java.net._
import java.sql._
import java.util._
import java.util.concurrent.TimeoutException

import fs2.Task
import io.circe.generic.auto._
import org.http4s.Status.Successful
import org.http4s._
import org.http4s.circe.jsonOf
import org.log4s.getLogger
import slick.jdbc.MySQLProfile.api._
import uber.db.Tables
import uber.git.GitConfiguration
import uber.rest.HistoryRestApi
import uber.utils.FutureUtils._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent._
import scala.concurrent.duration.Duration
import scala.util.{Random, Try}

object QueryDispatcher {

  /**
    * Build the history dispatcher service, the future completes when the service is correctly initialized.
    */
  def build(requestShutdown: => Unit)(implicit context: ServiceExecutionContext,
                                      conf: QueryServiceConfiguration,
                                      gitConf: GitConfiguration,
                                      dispatcherConf: QueryDispatcherConfiguration): Future[QueryDispatcher] = {

    import dispatcherConf._

    val hd = new QueryDispatcher(requestShutdown)
    hd.start().map(_ => hd)
  }

}

/**
  * A query service implementation that dispatches queries to workers.
  * The callback `requestShutdown` is called when the service requires the application to exit.
  */
class QueryDispatcher private (requestShutdown: => Unit)(implicit context: ServiceExecutionContext,
                                                         conf: QueryServiceConfiguration,
                                                         dispatcherConf: QueryDispatcherConfiguration)
    extends QueryService {

  import dispatcherConf._

  private val log = getLogger

  /**
    * A timer for updating the workers at regular intervals.
    */
  private val workersRefresh: Timer = new Timer(true)

  /**
    * List of the IP addresses of the workers.
    */
  private val workerAddresses: ArrayBuffer[String] = new ArrayBuffer()

  /**
    * Answers the history query by querying a query worker.
    */
  override def history(user: UserName, repo: RepoName, page: Int): Future[HistoryResponse] = {
    def performRequest() = {
      implicit val jsonDecoder: EntityDecoder[HistoryResponse] = jsonOf[HistoryResponse]
      attemptQuery[HistoryResponse](HistoryRestApi.makeHistoryUriRequest(user, repo, page), user, repo, RetryLater())
    }

    (performRequest _).retry({
      case e: NoRouteToHostException =>
        true // retries if the host is dead
      case e: Throwable =>
        false // otherwise just fail
    }, dispatcherConf.RETRIES)
  }

  /**
    * Attempt to perform the `requestUri` query on the best suited worker for the repository `repo` of `user`.
    * If the request timeout `retryLater` is returned.
    *
    * The future fails with `NoRouteToHostException` exception if the chosen endpoint appears to be dead.
    */
  private def attemptQuery[X <: QueryResponse](requestUri: Uri, user: UserName, repo: RepoName, retryLater: => X)(
    implicit jsonDecoder: EntityDecoder[X]
  ): Future[X] = {
    findBestWorker(user, repo).flatMap { workerAddress =>
      log.info(s"Query on repository $user/$repo will be served by $workerAddress")
      val worker = Uri.unsafeFromString(s"http://$workerAddress:${dispatcherConf.WORKERS_PORT}")
      val uri = HistoryRestApi.makeUri(worker, requestUri)
      context.httpClient
        .get(uri) {
          case Successful(resp) => resp.as[X]
          case response         => Task.fail(new RuntimeException(s"Bad response from $workerAddress: $response"))
        }
        .unsafeRunAsyncFuture()
        .recoverWith {
          case e: NoRouteToHostException =>
            log.error(s"No route to host $workerAddress")
            declareWorkerDead(workerAddress, user, repo).map {
              throw e
            }
          case e: TimeoutException =>
            log.error(s"Timeout for request $uri")
            Future.successful(retryLater)
        }
    }
  }

  /**
    * Find the best worker to serve the request for the repository `repo` of the `user`,
    * according to the knowledge in the db, otherwise a random one is picked.
    * The database is updated accordingly.
    */
  private def findBestWorker(user: UserName, repo: RepoName): Future[String] = {
    // pick a random one, this will be the one chosen if the db has not an entry for the given repository
    val random = findRandomWorker()

    // finding worker for the given repository in the db
    val requestAndUpdate = Tables.WorkersTable
      .filter(entry => entry.user === user.name && entry.repo === repo.name)
      .map(_.workerIp)
      .result
      .map(_.headOption)
      .flatMap { worker =>
        worker
          .map(DBIO.successful) // if a worker has been found, then return it
          .getOrElse { // otherwise update the db with the random one and return it
            log.info(s"Random worker $random chosen for repository $user/$repo")
            DBIO.seq(Tables.WorkersTable += (user.name, repo.name, random)).map(_ => random)
          }
      }
      .transactionally // perform the procedure transactionally

    // perform the entire procedure
    shutdownOnDbFailure(context.db.run(requestAndUpdate)).recover {
      case _: Throwable =>
        log.warn("Failed finding the best worker on the database and updating it, keep going with a random one, db not notified")
        findRandomWorker()
    }
  }

  /**
    * Picking a random worker from the ones available.
    */
  private def findRandomWorker(): String = this.synchronized {
    if (workerAddresses.isEmpty)
      requestShutdown
    workerAddresses(Random.nextInt(workerAddresses.size))
  }

  /**
    * Delete a worker from the list of workers and from the db.
    */
  private def declareWorkerDead(address: String, user: UserName, repo: RepoName): Future[_] = this.synchronized {
    log.info(s"Worker $address marked as dead")
    val idx = workerAddresses.indexOf(address)
    if (idx >= 0)
      workerAddresses.remove(idx)
    val removalRequest = Tables.WorkersTable
      .filter(entry => entry.user === user.name && entry.repo === repo.name)
      .delete
    shutdownOnDbFailure(context.db.run(removalRequest)).recover {
      case _: Throwable =>
        log.warn(s"Failed removing worker $address on the database for $user/$repo")
    }
  }

  /**
    * Updates the available workers.
    */
  private def updateWorkers(): Unit = {
    val addresses = InetAddress.getAllByName(dispatcherConf.WORKERS_HOSTNAME).map(_.getHostAddress)
    if (addresses.isEmpty) throw new UnknownHostException("Addresses not found for the workers")
    this.synchronized {
      val addressSet = addresses.toSet
      if (workerAddresses.toSet != addressSet) {
        log.info(s"Found the following ${addresses.length} workers for the query service ${dispatcherConf.WORKERS_HOSTNAME}: $addressSet")
      }
      workerAddresses.clear()
      workerAddresses ++= addresses
    }
  }

  /**
    * Start the dispatcher service.
    */
  private def start(): Future[_] = {
    // Building a first list of workers
    updateWorkers()

    // Initialize automatic refreshing of available workers
    val task = new TimerTask {
      override def run(): Unit =
        try {
          updateWorkers()
        } catch {
          case _: Throwable => requestShutdown
        }
    }
    workersRefresh.scheduleAtFixedRate(task, 0, dispatcherConf.WORKER_REFRESH_RATE.toMillis)

    // Init db schema, if not already done
    shutdownOnDbFailure(Tables.initSchemas(context.db))
      .recover {
        case e: SQLSyntaxErrorException if e.toString.contains("already exists") =>
          log.warn(s"Ignoring error $e during setup of db schema")
      }
  }

  /**
    * Shutdown the service whenever the future fails because of connectivity problems with the db.
    */
  private def shutdownOnDbFailure[X](future: Future[X]): Future[X] = {
    future.recover {
      case e: SQLTransientConnectionException =>
        log.error("Failed to connect to database, shutting down")
        requestShutdown
        throw e;
    }
  }

  private def shutdown(): Unit = {
    log.info("Query dispatcher goes down")
    workersRefresh.cancel()
    context.db.close()
  }

  override def finalize(): Unit = {
    shutdown()
  }
}

trait QueryDispatcherConfiguration {

  /**
    * The interval the workers IPs are refreshed.
    */
  def WORKER_REFRESH_RATE: Duration

  /**
    * Number of retries to perform to answer a request when a worker appears to be dead, and
    * number of retries before shutting down because no worker can be found.
    */
  def RETRIES: Int

  /**
    * Hostname to use to reach query service workers.
    */
  def WORKERS_HOSTNAME: String

  /**
    * Workers REST API port.
    */
  def WORKERS_PORT: Int

  implicit def ec: ExecutionContext
}
