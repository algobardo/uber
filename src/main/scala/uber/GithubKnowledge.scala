package uber

import java.net.URL

import uber.query_service.{RepoName, UserName}

object GithubKnowledge {

  val MAX_USER_LENGTH: Int = 40

  val MAX_REPO_LENGTH: Int = 100

  def getCloneUrl(user: UserName, repo: RepoName): URL = new URL(s"https://github.com/${user.name}/${repo.name}.git")

}
