package uber

import com.typesafe.config.ConfigFactory
import org.log4s.getLogger
import uber.git.GitConfiguration
import uber.query_service.{QueryDispatcherConfiguration, QueryServiceConfiguration}
import uber.rest.{HistoryRestServiceConf, ServerConfiguration}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}

case class Configuration(name: String = "application") {

  private val log = getLogger

  private val conf = ConfigFactory.load(name)

  log.info(s"Application configuration loaded: $name")

  private def getDuration(name: String): FiniteDuration = FiniteDuration(conf.getLong(name), "seconds")

  private def getInt(name: String): Int = conf.getInt(name)

  private def getString(name: String): String = conf.getString(name)

  case class Conf(override val GIT_CMD_TIMEOUT: FiniteDuration = getDuration("GIT_CMD_TIMEOUT"),
                  override val REST_SERVER_PORT: Int = getInt("REST_SERVER_PORT"),
                  override val REST_SERVER_HOST: String = getString("REST_SERVER_HOST"),
                  override val CACHED_REPOS: Int = getInt("CACHED_REPOS"),
                  override val WORKER_REFRESH_RATE: FiniteDuration = getDuration("WORKER_REFRESH_RATE"),
                  override val RETRIES: Int = getInt("RETRIES"),
                  override val PAGE_SIZE: Int = getInt("PAGE_SIZE"),
                  override val WORKERS_HOSTNAME: String = getString("WORKERS_HOSTNAME"),
                  override val WORKERS_PORT: Int = getInt("WORKERS_PORT"),
                  override val FETCH_INTERVAL: FiniteDuration = getDuration("FETCH_INTERVAL"),
                  override val TIMEOUT_RETRY_LATER: FiniteDuration = getDuration("TIMEOUT_RETRY_LATER"),
                  implicit override val ec: ExecutionContextExecutor = ExecutionContext.global)
      extends GitConfiguration
      with ServerConfiguration
      with QueryServiceConfiguration
      with HistoryRestServiceConf
      with QueryDispatcherConfiguration {
    log.info(s"Configuration created: $this")
  }
}
