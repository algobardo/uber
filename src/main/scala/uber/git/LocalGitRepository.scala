package uber.git

import java.net.URL
import java.nio.file._
import java.time.Instant

import org.apache.commons.io.FileUtils
import org.log4s.getLogger
import uber.utils.{CliExecutor, TemporaryResource, TemporaryResourceHandler}

import scala.concurrent._
import scala.concurrent.duration.Duration
import scala.util._

/**
  * An interface to the local git cli.
  */
object LocalGitRepository {

  private val log = getLogger

  /**
    * Clone a git repository locally.
    * If a `destination` folder is provided then the repository is cloned in that folder,
    * and the returned repository is not ephemeral.
    *
    * The future fails with `GitRepoDoesNotExists` if the remote repository does not exists.
    * The future fails with `GitCmdFailed` if the clone command fails.
    */
  def clone(url: URL, destination: Option[Path] = None)(implicit conf: GitConfiguration): Future[LocalGitRepository] = {
    import conf._
    existsRemote(url).flatMap {
      case true =>
        val ephemeral = destination.isEmpty
        val destinationDir = destination.getOrElse(Files.createTempDirectory("git"))
        val repo = LocalGitRepository(url, destinationDir, ephemeral)

        log.info(s"Cloning $url in ${repo.p}")
        val cmd = List("git", "clone", url.toString, repo.p.toAbsolutePath.toString)
        CliExecutor
          .executeCommand(cmd, repo.p, conf.GIT_CMD_TIMEOUT)
          .map(r => repo)
          .recoverWith {
            case _: Throwable => throw GitCmdFailed(s"git clone for $url failed")
          }
      case false => Future.failed(GitRepoDoesNotExists())
    }
  }

  /**
    * Create a non ephemeral local git repository from the given folder.
    *
    * The future fails with GitCmdFailed if `path` is not a valid git repository.
    */
  def fromFolder(path: Path)(implicit conf: GitConfiguration): Future[LocalGitRepository] = {
    import conf._
    Future {
      Files.exists(path)
    }.flatMap { exists =>
      if (!exists) throw GitCmdFailed(s"Provided directory does not exists")
      val candidateRepo = LocalGitRepository(null, path, ephemeral = false)
      candidateRepo.remote("origin").map { url =>
        Try(new URL(url))
          .recover { case _: Throwable => throw GitCmdFailed("Url provided by the repository is not valid") }
          .map(validatedUrl => candidateRepo.copy(url = validatedUrl))
          .get
      }
    }
  }

  /**
    * Returns a true in the future iff if the remote exists.
    */
  private def existsRemote(remote: URL)(implicit conf: GitConfiguration): Future[Boolean] = {
    import conf._
    log.info(s"Checking whether the remote $remote exists")
    val cmd = List("git", "ls-remote", remote.toString)
    val result = CliExecutor.executeCommand(cmd, Paths.get(""), conf.GIT_CMD_TIMEOUT, throwOnErrorCode = false)
    result.map(_.code == 0)
  }
}

/**
  * A cloned local git repository.
  * If `ephemeral` then the folder is removed whenever the repository is GCed or the JVM is shut down.
  */
case class LocalGitRepository private (url: URL, p: Path, ephemeral: Boolean = false)(implicit conf: GitConfiguration)
    extends TemporaryResource[Path] {

  private var lastFetch: Option[Instant] = None

  if (ephemeral)
    TemporaryResourceHandler.registerForCleanup(this)

  import LocalGitRepository._
  import conf._

  /**
    * Returns the instant the last fetch has been completed successfully.
    */
  def getLastFetch: Option[Instant] = lastFetch

  /**
    * Fetch the the remote of the repository.
    *
    * The future fails with GitCmdFailed if the fetch command fails.
    */
  def fetch(): Future[LocalGitRepository] = {
    log.info(s"Fetching new data for $this")
    val cmd = List("git", "fetch")
    val result = CliExecutor.executeCommand(cmd, p, conf.GIT_CMD_TIMEOUT, throwOnErrorCode = true)
    result
      .map { _ =>
        lastFetch = Some(Instant.now())
        this
      }
      .recoverWith {
        case error: Throwable =>
          throw GitCmdFailed(s"git fetch in $p failed", error)
      }
  }

  /**
    * Retrieves the remote `name`.
    *
    * The future fails with GitCmdFailed if the remote command fails.
    */
  def remote(name: String): Future[String] = {
    log.info(s"Retrieving remote $name of $this")
    val cmd = List("git", "remote", "get-url", "origin")
    val result = CliExecutor.executeCommand(cmd, p, conf.GIT_CMD_TIMEOUT, throwOnErrorCode = true)
    result
      .map(_.log.trim)
      .recoverWith {
        case error: Throwable =>
          throw GitCmdFailed(s"git remote get-url failed", error)
      }
  }

  /**
    * List the last `howMany` commits starting from `from`, most recent first.
    *
    * The future fails with GitCmdFailed if the listing fails.
    */
  def listCommits(from: Int, howMany: Int = Int.MaxValue): Future[List[GitCommit]] = {
    log.info(s"Retrieving commits for $this")
    // listing commits in the format sha,message
    val cmd = List("git", "log", "--all", "-n", howMany.toString, "--skip", from.toString, "--pretty=format:%H,%s")
    CliExecutor
      .executeCommand(cmd, p, conf.GIT_CMD_TIMEOUT, throwOnErrorCode = true)
      .map(_.log.trim.lines)
      .map { lines =>
        lines.map { line =>
          // extracting sha and message
          val separator = line.indexOf(",")
          val sha = line.substring(0, separator)
          val message = line.substring(separator + 1)
          GitCommit(sha, message.trim)
        }.toList
      }
  }

  override def finalize(): Unit = {
    if (ephemeral)
      cleanup()
  }

  /**
    * Cleanup the folder where this repository is cloned.
    */
  override def cleanup(): Unit = {
    log.info(s"Deleting $p")
    TemporaryResourceHandler.unregisterForCleanup(this)
    try {
      FileUtils.deleteDirectory(p.toFile)
    } catch {
      case e: Throwable =>
        log.error(e)(s"Deletion of $p failed")
    }
  }
}

trait GitConfiguration {

  /**
    * Timeout for the git command.
    */
  def GIT_CMD_TIMEOUT: Duration

  implicit def ec: ExecutionContext
}

case class GitCmdFailed(msg: String, cause: Throwable = null) extends RuntimeException(msg, cause)

case class GitRepoDoesNotExists() extends RuntimeException()
