package uber.git

/**
  * A git commit.
  */
case class GitCommit(sha: String, message: String) {

  if (sha.length != 40) throw new RuntimeException(s"Invalid SHA: $sha")

}
