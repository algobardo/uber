package uber.rest

import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.dsl.{IntVar, Root, _}
import org.http4s.{HttpService, Uri}
import uber.query_service.{QueryService, RepoName, UserName}
import uber.rest.Sanitization.ValidName

import scala.concurrent.ExecutionContext

/**
  * A REST interface to the query service
  */
class HistoryRestApi(historyService: QueryService)(implicit val serverConf: HistoryRestServiceConf) {

  import serverConf._

  val httpService = HttpService {
    case GET -> Root / "history" / ValidName(user) / ValidName(repo) =>
      Ok(historyService.history(UserName(user), RepoName(repo), 0).map(_.asJson))
    case GET -> Root / "history" / ValidName(user) / ValidName(repo) / IntVar(page) =>
      Ok(historyService.history(UserName(user), RepoName(repo), page).map(_.asJson))
  }

}

object HistoryRestApi {

  def makeHistoryUriRequest(user: UserName, repo: RepoName, page: Int): Uri = Uri.unsafeFromString(s"/history/$user/$repo/$page")

  def makeUri(endpoint: Uri, request: Uri): Uri = endpoint.resolve(request)
}

trait HistoryRestServiceConf {
  implicit def ec: ExecutionContext
}
