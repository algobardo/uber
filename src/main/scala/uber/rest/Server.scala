package uber.rest

import fs2.Task
import org.http4s.HttpService
import org.http4s.server.blaze.BlazeBuilder
import org.http4s.util.StreamApp
import uber.git.GitConfiguration

trait Server extends StreamApp {

  implicit val conf: ServerConfiguration
  val historyService: HttpService

  def stream(args: List[String]): fs2.Stream[Task, Nothing] =
    BlazeBuilder
      .bindHttp(conf.REST_SERVER_PORT, conf.REST_SERVER_HOST)
      .mountService(historyService, "/")
      .serve
}

trait ServerConfiguration extends GitConfiguration with HistoryRestServiceConf {
  def REST_SERVER_HOST: String
  def REST_SERVER_PORT: Int
}
