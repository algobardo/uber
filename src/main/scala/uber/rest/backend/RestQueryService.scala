package uber.rest.backend

import org.http4s.server.middleware.CORS
import uber.Configuration
import uber.query_service.QueryServiceImpl
import uber.rest.{HistoryRestApi, Server}

object RestQueryService extends Server {
  implicit val conf = Configuration().Conf()
  val history = new HistoryRestApi(new QueryServiceImpl())
  val historyService = CORS(history.httpService)
}
