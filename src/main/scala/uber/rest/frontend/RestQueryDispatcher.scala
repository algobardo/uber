package uber.rest.frontend

import org.http4s.server.middleware.CORS
import uber._
import uber.query_service.{QueryDispatcher, RealWorldExecutionContext}
import uber.rest.{HistoryRestApi, Server}

import scala.concurrent.Await
import scala.concurrent.duration._

object RestQueryDispatcher extends Server {
  implicit val conf = Configuration().Conf()
  implicit val context = RealWorldExecutionContext
  val historyDispatcher = try { Await.result(QueryDispatcher.build(this.requestShutdown.unsafeRun()), 20.seconds) } catch {
    case e: Throwable => this.requestShutdown.unsafeRun(); throw e;
  }
  val historyService = CORS(new HistoryRestApi(historyDispatcher).httpService)
}
