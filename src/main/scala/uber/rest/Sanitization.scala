package uber.rest

object Sanitization {
  object ValidName {
    def unapply(str: String): Option[String] = {
      val validPattern = "[a-zA-Z0-9_-]*".r
      str match {
        case validPattern() => Some(str)
        case _              => None
      }
    }
  }
}
