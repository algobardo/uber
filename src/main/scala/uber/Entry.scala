package uber

import uber.rest.backend.RestQueryService
import uber.rest.frontend.RestQueryDispatcher

object Entry extends App {

  val cmd = System.getenv("START_CMD") match {
    case null => "history-worker"
    case s    => s
  }
  println(s"Starting $cmd")

  cmd match {
    case "history-dispatcher" =>
      RestQueryDispatcher.main(Array())
    case "history-worker" =>
      RestQueryService.main(Array())
    case x =>
      println(s"Unrecognized $x")
  }
}
