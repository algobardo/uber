package uber

import org.log4s.getLogger
import org.scalatest.AsyncFlatSpec
import uber.query_service._

import scala.concurrent.Future

class HistoryServiceTest extends AsyncFlatSpec {

  private implicit val conf = Configuration().Conf()

  private val log = getLogger

  it should "should return a list of commits with the correct number of elements" in {
    val git = new QueryServiceImpl()
    for {c2 <- git.history(UserName("Homebrew"), RepoName("formula-patches"), 7)} yield {
      c2 match {
        case HistorySuccess(list) =>
          log.info(s"Retrieved: ${list.map(_.sha)}")
          assert(list.lengthCompare(conf.PAGE_SIZE) == 0)
        case NonExistingRepo(_) | RetryLater() =>
          assert(false)
      }
    }
  }

  it should "succeeds looking up 0 commits" in {
    val git = new QueryServiceImpl()
    git.history(UserName("Homebrew"), RepoName("formula-patches"), 1223232).map {
      case HistorySuccess(list) =>
        log.info(s"Retrieved: $list")
        assert(list.lengthCompare(0) == 0)
      case NonExistingRepo(_) | RetryLater() =>
        assert(false)
    }
  }

  it should "querying twice should put elements in the cache" in {
    val git = new QueryServiceImpl()
    val request = git.history(UserName("Homebrew"), RepoName("formula-patches"), 12)
    val requestOnCachedRepo = git.history(UserName("Homebrew"), RepoName("formula-patches"), 12)
    log.info(s"Two futures (possibly) waiting to be completed: $request and $requestOnCachedRepo")
    Future.sequence(List(request, requestOnCachedRepo)).map {_ =>
      assert(git.cacheSize == 1)
    }
  }

  it should "maintain a correctly sized cache" in {
    implicit val conf = Configuration().Conf(CACHED_REPOS = 2)
    val git = new QueryServiceImpl()
    Future.sequence(List(git.history(UserName("Homebrew"), RepoName("brew"), 12),
      git.history(UserName("Homebrew"), RepoName("homebrew-core"), 12),
      git.history(UserName("Homebrew"), RepoName("install"), 45),
      git.history(UserName("Homebrew"), RepoName("formula-patches"), 11),
      git.history(UserName("Homebrew"), RepoName("homebrew-science"), 18),
      git.history(UserName("Homebrew"), RepoName("brew"), 80))).map { _ =>
      assert(git.cacheSize <= 2)
    }
  }

}
