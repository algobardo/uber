package uber

import java.net.URL
import java.nio.file.Files
import java.time.{Duration, Instant}

import org.log4s.getLogger
import org.scalatest.AsyncFlatSpec
import uber.git.LocalGitRepository

class GitCmdTest extends AsyncFlatSpec {

  private val log = getLogger

  private implicit val conf = Configuration().Conf()

  val repo1 = LocalGitRepository.clone(new URL("https://github.com/algobardo/foobar.git"))
  val repo2 = LocalGitRepository.clone(new URL("https://github.com/Homebrew/brew.git"))

  it should "be able to clone a public repo" in {
      repo1.map { repo =>
        log.info(s"Checking existance of ${repo.p}")
        assert(Files.exists(repo.p))
      }
  }

  it should "listing commits works" in {
    repo1.flatMap { repo =>
      repo.listCommits(0)
    }.map{ commits =>
      log.info(s"Retrieved commits: $commits")
      assert(commits.nonEmpty && commits.lengthCompare(1) == 0)
    }
  }

  it should "listing commits on big repo works" in {
    var start: Instant = null
    var end: Instant = null
    repo2
      .flatMap { repo =>
        start = Instant.now()
        repo.listCommits(0)
      }.map { commits =>
      end = Instant.now()
      log.info(s"Retrieved commits: ${commits.size} in ${Duration.between(start, end).toMillis}ms")
      assert(commits.nonEmpty && commits.lengthCompare(10000) > 0)
    }
  }

  it should "listing range of commits on big repo works" in {
    var start: Instant = null
    var end: Instant = null
    repo2
      .flatMap { repo =>
        start = Instant.now()
        repo.listCommits(100, 100)
      }.map { commits =>
      end = Instant.now()
      log.info(s"Retrieved commits: ${commits.size} in ${Duration.between(start, end).toMillis}ms")
      assert(commits.nonEmpty && commits.lengthCompare(100) == 0)
    }
  }

  it should "fetching works" in {
    repo1.flatMap { repo =>
      repo.fetch()
    }.map(_ => assert(true))
  }

  it should "create a local git from a folder" in {
    repo1.flatMap{repo =>
      LocalGitRepository.fromFolder(repo.p).map(localRepo => assert(localRepo.url.toString == repo.url.toString))
    }
  }
}
