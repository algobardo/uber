name := "uber"

version := "0.1"

scalaVersion := "2.12.4"

val Http4sVersion = "0.17.6"
val LogbackVersion = "1.2.3"

// REST API dependencies
libraryDependencies ++= Seq(
  "org.http4s"      %% "http4s-blaze-server" % Http4sVersion,
  "org.http4s"      %% "http4s-blaze-client" % Http4sVersion,
  "org.http4s"      %% "http4s-circe"        % Http4sVersion,
  "org.http4s"      %% "http4s-dsl"          % Http4sVersion,
  "ch.qos.logback"  %  "logback-classic"     % LogbackVersion
)

// JSON library
libraryDependencies += "io.circe" %% "circe-generic" % "0.8.0"
libraryDependencies += "io.circe" %% "circe-literal" % "0.8.0"

// File handling utilities
libraryDependencies += "commons-io" % "commons-io" % "2.6"

// Slick dependency
libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.2.1",
  "org.slf4j" % "slf4j-api" % "1.6.4",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.2.1",
  "mysql" % "mysql-connector-java" % "5.1.34"
)

libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.0.4" % "test"

mainClass in (Compile, run) := Some("uber.Entry")

mainClass in assembly := Some("uber.Entry")
assemblyJarName in assembly := "history.jar"
test in assembly := {}