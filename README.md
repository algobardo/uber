
As suggested by the recruiter, I felt 
> free to do an alternative project in the same spirit if you know a good data source

I have chosen the **Back-end track**: 
>include a minimal front-end (e.g. a static view or API docs). 
Write, document and test your API as if it will be used by other services.


In my work at au.dk I faced the problem of "querying github" at least once a year.
Because of the known quota limitations of the Github API, in some cases I ended up parsing 
Github HTML pages to save requests quota to get out the data I needed in a reasonable time.
Unfortunately this is not an ideal solution, also it cannot be generalized to all kind of queries.
What if I just want to run `cloc` on the repository? 
What if I want to perform the same query on github, bitbucket and gitlab? Should three different APIs and HTML pages be queried? 

This project attempts to solve this problem with a fresh perspective.

Github Query Service
=======================

This service allows to perform queries on repositories hosted on Github, 
without using any of the Github REST API, and hence without inheriting any 
of its quota limitations. 
**Note: The only query supported at the moment is retrieving the commit history of a project.**

### Design

For performance, queries are answered by using local clones of Github repositories. 
Repositories are cloned on-demand on the machine answering the query. 
For scalability, the service for answering queries can replicated on multiple machines, acting as *workers*.
To guarantee a good service level, a *query dispatcher* attempts to dispatch queries to machines 
where a repository copy is already available, if possible.

#### Main Components


The web application is in three tier.
The *frontend* is the the REST API of the query dispatcher service, possibly served by multiple servers. 
The *backend* is made by a set of standalone workers that provide the query service.
There is not a true *data tier*, as data is only temporarily stored on disk in the form of git repositories.
The query dispatcher uses a database for coordinating its instances.

The main components are listed below. 


- A small facade around the git CLI is available in the class `LocalGitRepository`. 
An instance of a `LocalGitRepository` effectively represent a git repository cloned on 
some temporary folder. An instance of `LocalGitRepository` can be ephemeral or not. 
Ephemeral ones cause the temporary folder to be deleted whenever the instance is GCed, 
to save disk space.

- The public interface of the query service, including the types of the possible responses, 
is in the trait `QueryService`. Two implementations of the query service are available: 
`QueryServiceImpl` and `QueryDispatcher`.

- A standalone implementation of the query service is in the `QueryServiceImpl` class.
The service maintain a LRU cache of cloned repositories.

- A dispatching implementation of the query service is in the `QueryDispatcher` class.
The query dispatcher forwards the queries to a set of workers that implement a REST API for the query service.
Workers are discovered through DNS and periodically updated.
Whenever a query concerning a given repository is served by a worker, such knowledge is recorded in the database, 
so allowing subsequent queries on the same repository to be resolved by the same worker.
The database allows the query service to be served by multiple query dispatchers.

- A typical deployment on a docker swarm is described in `docker/docker-swarm.yml`.

- A minimal web-ui for querying the commits history of a repository is in `html/index.html`.

#### Consideration on the Current Design

I decided on-purpose to avoid using external frameworks that could have simplified part of this project.
When possible I went minimal, coding myself a decent solution to leave some meat to evaluate (error handling, 
interface design, etc.), instead of a sea of glue code.
For example I avoided using existing git libraries.

I also avoided using Akka Cluster, Apache Mesos or any of the many distributed task queues for the query dispatching part.
Note however that the design of services has been inspired by actors: services fail whenever they encounter 
a situation that they cannot handle, expecting the supervisor (in this case the docker swarm) to respawn them. 

Also for the web-ui I avoided on-purpose to throw in React or Angular or any other heavyweight framework.
Features such as two-way data binding, paging, routing, etc. are very easy to obtain when using such frameworks.

A reasonable level of balancing could probably also be achieved by replacing the query dispatcher by 
a HAProxy in front of the standalone workers, but note that with the current design query dispatcher can be
scaled, and smarter dispatching policies could be implemented in the query service itself.
For example the query balancer could use knowledge on the the repository size to answer queries 
 using the Github API until one of the worker cannot serve queries on the repository fast enough.


### REST API

#### History
```
GET /history/:user/:repo/:page
```

Returns the `page`-th page in the list of commits of the repository `repo` of the `user`, ordered from the most recent to oldest.

```json
{
  "HistorySuccess": {
    "commits": [
      {"sha": "78933b3ee91a9bfd71128bd7e7e46b9af9ef30e2", "message": "..."},
      {"sha": "78933b3ee91a9bfdd8374akdf7e46b9af93f30e2", "message": "..."}  
    ]
  }
}
```

If the repository does not exist, then the following response is returned 

```json
{
  "NonExistingRepo": {
    "msg": "..."
  }
}
```

If the request is taking too much time, because the repository still has to be cloned and it is big, 
the client is suggested to re-retry later by the following response:

```json
{
  "RetryLater": {}
}
```


### TODO

- Add more kinds of queries on repository.
- If the query can be answered by the Github API and the worker is taking too much time, then fallback on the GitHub API
- Dispatcher should dispatch the query to multiple machines if a repository become hot.
- Anti-abuse mechanisms?
- Query dispatcher should maintain a cache of resolved queries, to avoid querying the db for every request.
- The size of cloned repository cache should be based on disk space available. 
  rather than on the number of cloned repositories.
- Add tests for the query dispatcher by providing different `ServiceExecutionContext` than the normal one.


### Demo

A running instance of this service has been deployed on Google Compute Engine and served at http://35.204.106.25/

The following configuration has been used: 1 database server, 2 query dispatchers, 4 query workers. 
*Note: To contain costs, deployment is a swarm with only one virtual machines with 1vCPU, 1.7Gb RAM, 30Gb disk, be gentle!*

The underlying REST API of the query service is accessible at the address http://35.204.106.25:8080.
A sample query, returning the last commits of the `Homebrew/brew` repository is http://35.204.106.25:8080/history/Homebrew/brew.

*Note: Since repositories still need to be cloned, it will still take a lot of time to answer the first query for big 
repository, such as torvalds/linux*

## Building

Ensure you have the following dependencies: 

- make
- git (tested with 2.14.1)
- sbt
- scala 2.12.*


To build the project

```
make
``` 

To run the tests

```
make tests
``` 

## Deploying

First, publish the images on docker hub
```
docker-publish
```
*Note: you may need to change the username and the image name written in `Makefile` and `docker-swarm.yml`* 

Then, on a docker swarm manager run 

```
make start-stack
```

If you don't have a docker swarm manager, just make one:
```
docker swarm init
```

Once the stack is started, the REST service should be deployed and running on the swarm.

